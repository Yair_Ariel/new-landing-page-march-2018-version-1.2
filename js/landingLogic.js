var accordions = document.getElementsByClassName("accordion");

for (var i = 0; i < accordions.length; i++) {
  accordions[i].onclick = function() {
    this.classList.toggle('is-open');

    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      // accordion is currently open, so close it
      content.style.maxHeight = null;
    } else {
      // accordion is currently closed, so open it
      content.style.maxHeight = content.scrollHeight + "px";
    }
  }
}

var arrows = document.getElementsByClassName("down-arrow");
for (var i = 0; i < arrows.length; i++) {
  arrows[i].onclick = function() {
    this.classList.toggle('is-open');
  }
}

var swiper = new Swiper('.brands-swiper-container', {
    slidesPerView: 2.5,
    spaceBetween: 5,
    loop: true
});

var buisnessesSwiper = new Swiper('.businesses-swiper-container', {
  slidesPerView: 1.5,
  spaceBetween: 5,
  loop: true,
});

var testimonialsSwiper = new Swiper('.testimonials-swiper-container', {
  slidesPerView: 1.4,
  spaceBetween: 30,
  loop: true
});

/* Video */
    /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = "https://www.youtube.com/embed/dRbc_uahYxY?rel=0&amp;showinfo=0&autoplay=1";
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#videoModal").on('hide.bs.modal', function(){
        $("#beam-video").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#videoModal").on('show.bs.modal', function(){
        $("#beam-video").attr('src', url);
    });
/* Video */

$(".link-to-first-contact").click(function() {
  $('html,body').animate({
      scrollTop: $("#row-inline-contact").offset().top},
      300);
});

$(".link-to-contact").click(function() {
  $('html,body').animate({
      scrollTop: $("#row-contact").offset().top},
      2500);
  
  setTimeout(function() {
    $(_hbspt3 + " input[name=firstname]").focus()
  }, 2600);
});

function GetPhoneType(_phone, _control){
  // verify phone number via AJAX call
  var phone_number = '1' + _phone;
  //_control.val("Invalid phone");
  
  $.ajax({
      url: 'http://apilayer.net/api/validate?access_key=d5099f0446572170314e23051ae7a354&number=' + phone_number,   
      dataType: 'jsonp',
      success: function(json) { _control.val(json.line_type); }
  });
}

